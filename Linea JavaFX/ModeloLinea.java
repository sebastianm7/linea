
/**
 * Un ejemplo que modela un Linea usando POO
 *
 * @author (Milton JesÃºs Vera Contreras - miltonjesusvc@ufps.edu.co)
 * @version 0.000000000000001 :) --> Math.sin(Math.PI-Double.MIN_VALUE)
 */
public class ModeloLinea {

    float x1;
    float x2;
    float y1;
    float y2;

    public ModeloLinea() {
    }

    public ModeloLinea(float x1, float y1, float x2, float y2) {
        this.x1 = x1;
        this.x2 = x2;
        this.y1 = y1;
        this.y2 = y2;
    }

    public float getX1() {
        return x1;
    }

    public void setX1(float x1) {
        this.x1 = x1;
    }

    public float getX2() {
        return x2;
    }

    public void setX2(float x2) {
        this.x2 = x2;
    }

    public float getY1() {
        return y1;
    }

    public void setY1(float y1) {
        this.y1 = y1;
    }

    public float getY2() {
        return y2;
    }

    public void setY2(float y2) {
        this.y2 = y2;
    }

    public String getUbicacion() {

        String ubicacion = "Error, es imposible";

        if (this.getCuantosCuadrantes() == 1) {
            ubicacion = "Esta en 1 cuadrante";
        } else if (this.getCuantosCuadrantes() == 2) {
            ubicacion = "Esta en 2 cuadrantes";
        } else if (this.getCuantosCuadrantes() == 3) {
            ubicacion = "Esta en 3 cuadrantes";
        } else if (this.getCuantosCuadrantes() == 4) {
            ubicacion = "Esta en 4 Cuadrantes";
        } else {
            ubicacion = "No esta en ningun cuadrante";
        }
        //Complete para que la salida se lo esperado por los test...

        return ubicacion;
    }//fin getUbicacion

    public int getCuantosCuadrantes() {
        int i = 0;
        if(this.estaEnCuadranteI()){
            i++;
        }
        if(this.estaEnCuadranteII()){
            i++;
        }
        if(this.estaEnCuadranteIII()){
            i++;
        }
        if(this.estaEnCuadranteIV()){
            i++;
        }
        
        return i;//complete
    }//fin getCuantosCuadrantes

    public boolean estaEnCuadranteI() {
        if (this.getX1() > 0 && this.getY1() > 0 || this.getX2() > 0 && this.getY2() > 0) {
            return true;
        } 
        else if(this.getIntersectoY()>0){
            return true;
        }
            else {
            return false;/*complete*/
        }
    }//fin estaEnCuadranteI

    public boolean estaEnCuadranteII() {
        if (this.getX1() < 0 && this.getY1() > 0 || this.getX2() < 0 && this.getY2() > 0) {
            return true;
        } 
        else if(this.getIntersectoY()>0){
            return true;
        }
        else{
            return false;/*complete*/
        }
    }//fin estaEnPrimerCuadranteII

    public boolean estaEnCuadranteIII() {
        if (this.getX1() < 0 && this.getY1() < 0 || this.getX2() < 0 && this.getY2() < 0) {
            return true;
        }
        else if(this.getIntersectoY()<0){
            return true;
        }    
            else {
            return false;/*complete*/
        }
    }//fin estaEnCuadranteIII

    public boolean estaEnCuadranteIV() {
        if (this.getX1() > 0 && this.getY1() < 0 || this.getX2() > 0 && this.getY2() < 0) {
            return true;
        } 
        else if(this.getIntersectoY()<0){
            return true;
        }
        else {
            return false;/*complete*/
        }
    }//fin estaEnCuadranteIV

    public float getPendiente() {
        float m;
        m = (y2 - y1) / (x2 - x1);
        return m;/*complete*/
    }

    public float getIntersectoY() {
        float b;
        b = this.getY1() - this.getPendiente() * this.getX1();
        return b;/*complete*/
    }
}//fin clase Linea

