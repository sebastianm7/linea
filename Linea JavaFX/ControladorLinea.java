import javafx.scene.control.Button;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;

public class ControladorLinea {

    @FXML
    private Label lblUbicacion;

    @FXML
    private ImageView imageLinea;

    @FXML
    private BorderPane panel1;

    @FXML
    private Button cmdCoordenadas;

    @FXML
    private Label lblTitulo;

    @FXML
    private GridPane panelDatos;

    @FXML
    private Button cmdAyuda;

    @FXML
    private Label lblY1;

    @FXML
    private Label lblX1;

    @FXML
    private Label lblY2;

    @FXML
    private Label lblX2;

    @FXML
    private TextField txtX2;

    @FXML
    private TextField txtY1;

    @FXML
    private TextField txtX1;

    @FXML
    private TextField txtY2;

    @FXML
    private GridPane panelUbicacion;

    @FXML
    private Button cmdUbicacion;

    @FXML
    private GridPane panelBotones;
    
    private ModeloLinea linea;
    
    public ControladorLinea(){
    linea = new ModeloLinea();
    }
    
    @FXML
    void actualizarCoordenadas() {
    linea.setX1(Float.parseFloat(txtX1.getText()));
    linea.setX2(Float.parseFloat(txtX2.getText()));
    linea.setY1(Float.parseFloat(txtY1.getText()));
    linea.setY2(Float.parseFloat(txtY2.getText()));
    
    Alert alert = new Alert(Alert.AlertType.INFORMATION, "Coordenadas actualizadas");
    alert.showAndWait();
    }

    @FXML
    void determinarUbicacion() {
    lblUbicacion.setText(linea.getUbicacion());
    }

    @FXML
    void ayuda() {
    Alert alert = new Alert(Alert.AlertType.INFORMATION, "Determinar ubicación de línea con respecto al plano");
    alert.showAndWait();
    }

}
